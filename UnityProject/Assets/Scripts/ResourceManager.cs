using UnityEngine;

public static class ResourceManager
{
    public static T GetObjectOfType<T>(GameObject obj, Vector2 pos) where T : Component
    {
        return GetObject(obj, pos).GetComponent<T>();
    }

    public static GameObject GetObject(GameObject obj)
    {
        return GetObject(obj, Vector2.zero);
    }

    public static GameObject GetObject(GameObject obj, Vector2 pos)
    {
        return Object.Instantiate(obj, pos, Quaternion.identity);
    }

    public static Pawn GetPawn()
    {
        return Object.Instantiate(Resources.Load<GameObject>("Prefabs/Pieces/Pawn"), Vector3.zero, Quaternion.identity).GetComponent<Pawn>();
    }

    public static Queen GetQueen()
    {
        return Object.Instantiate(Resources.Load<GameObject>("Prefabs/Pieces/Queen"), Vector3.zero, Quaternion.identity).GetComponent<Queen>();
    }
}
