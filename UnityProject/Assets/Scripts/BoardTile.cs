using UnityEngine;

public class BoardTile : MonoBehaviour
{
    public Piece piece;
    [SerializeField]
    private GameObject viabilityIndicator;

    private void Awake()
    {
        EventManager.pieceMoved.AddListener(PieceMoved);
        EventManager.kingDied.AddListener((val) => KingDied());
        EventManager.startGame.AddListener(GameStarted);

    }

    private void OnMouseDown()
    {
        EventManager.clickedOnTile.Invoke(this);
    }

    private void OnMouseEnter()
    {
        EventManager.enterTile.Invoke(this);
    }

    private void OnMouseExit()
    {
        EventManager.exitTile.Invoke(this);
    }

    private void KingDied()
    {
        GetComponent<BoxCollider2D>().enabled = false;
    }

    private void GameStarted()
    {
        GetComponent<BoxCollider2D>().enabled = true;
    }

    private void PieceDied(Piece deathPiece)
    {
        if (deathPiece == piece)
            piece = null;
    }

    private void PieceMoved(Piece movedPiece, BoardTile tile)
    {
        if (movedPiece == piece && tile != this)
            piece = null;
    }

    public void Highlight(bool state)
    {
        viabilityIndicator.SetActive(state);
    }
}
