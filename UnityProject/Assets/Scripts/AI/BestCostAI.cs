using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestCostAI : MonoBehaviour
{
    public BoardSettings settings;
    private Board board;
    private List<Piece> pieces = new List<Piece>();
    private bool thinking = false;
    private float timeOut = 1f;

    private Piece bestPiece;
    private BoardTile bestTile;
    public bool debug = false;

    private float minMoveInterval = 1f;
    private float maxMoveInterval = 5f;
    private float moveTime = 1;

    private void Awake()
    {
        EventManager.pieceDied.AddListener(PieceDied);
        EventManager.kingDied.AddListener((val) => Stop());
        EventManager.boardInited.AddListener(Init);
    }

    private void Start()
    {
        board = Board.Instance;
        enabled = false;
    }

    private void Update()
    {
        if (debug)
        {
            if (Input.GetKeyDown(KeyCode.Space))
                MovePiece();
        }
        else
        {
            if (moveTime < 0)
            {
                moveTime = UnityEngine.Random.Range(minMoveInterval, maxMoveInterval);
                MovePiece();
            }
            else
                moveTime -= Time.deltaTime;
        }
    }

    private void Init()
    {
        pieces.Clear();

        foreach (Piece p in FindObjectsOfType<Piece>())
        {
            if (p.side != settings.PlayerSide)
                pieces.Add(p);
        }

        enabled = true;
    }

    private void Stop()
    {
        StopCoroutine(MovePieceCoroutine());
        thinking = false;
        enabled = false;
    }

    private void MovePiece()
    {
        if (!thinking && pieces.Count > 0)
            StartCoroutine(MovePieceCoroutine());
    }

    private IEnumerator MovePieceCoroutine()
    {
        thinking = true;

        int maxCost = 0;

        bestPiece = null;
        bestTile = null;

        for (int i = 0; i < pieces.Count; i++)
        {
            if (pieces[i].cooldownTime > 0) continue;
            int[][] pos = pieces[i].PossiblePositions();
            for (int j = 0; j < pos.GetLength(0); j++)
            {
                Piece p = board.tiles[pos[j][0], pos[j][1]].piece;
                if (p && p.side == settings.PlayerSide && p.cost > maxCost)
                {
                    maxCost = p.cost;
                    bestPiece = pieces[i];
                    bestTile = board.tiles[pos[j][0], pos[j][1]];
                }
            }
        }

        if (!bestPiece)
        {
            timeOut = 1;
            while (timeOut > 0)
            {
                Piece p = pieces.PickRandom();

                int[][] moves = p.PossiblePositions();
                if (moves.Length > 0)
                {
                    int[] coords = moves.PickRandom();
                    BoardTile tile = board.tiles[coords[0], coords[1]];
                    if (tile.piece) tile.piece.Die();
                    p.Move(tile);
                    thinking = false;
                    yield break;
                }
                timeOut -= Time.deltaTime;
                if (timeOut < 0)
                {
                    Debug.LogWarning("Time out! Couldn't chose a piece to move!");
                    thinking = false;
                    yield break;
                }
                yield return null;
            }
        }
        else
        {
            bestTile.piece.Die();
            bestPiece.Move(bestTile);
        }

        thinking = false;

    }

    private void PieceDied(Piece p)
    {
        if (pieces.Contains(p))
            pieces.Remove(p);
    }

}
