using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAI : MonoBehaviour
{
    public BoardSettings settings;
    public bool debug = false;
    private Board board;

    private List<Piece> pieces = new List<Piece>();
    private float timeOut = 1f;
    private bool thinking = false;

    private float minMoveInterval = 0.25f;
    private float maxMoveInterval = 1f;
    private float moveTime = 0;

    private void Awake()
    {
        EventManager.pieceDied.AddListener(PieceDied);
        EventManager.kingDied.AddListener((val) => Stop());
        EventManager.boardInited.AddListener(Init);
    }

    private void Start()
    {
        board = Board.Instance;
        enabled = false;
    }

    private void Init()
    {
        pieces.Clear();

        foreach (Piece p in FindObjectsOfType<Piece>())
        {
            if (p.side != settings.PlayerSide)
                pieces.Add(p);
        }

        enabled = true;
    }

    private void Update()
    {
        if (debug)
        {
            if (Input.GetKeyDown(KeyCode.Space))
                MovePiece();
        }
        else
        {
            if (moveTime < 0)
            {
                moveTime = Random.Range(minMoveInterval, maxMoveInterval);
                MovePiece();
            }
            else
                moveTime -= Time.deltaTime;
        }
    }

    private void MovePiece()
    {
        if (!thinking && pieces.Count > 0)
            StartCoroutine(MovePieceCoroutine());
    }

    private IEnumerator MovePieceCoroutine()
    {
        thinking = true;
        timeOut = 1;
        while (timeOut > 0)
        {
            Piece p = pieces.PickRandom();

            int[][] moves = p.PossiblePositions();
            if (moves.Length > 0)
            {
                int[] coords = moves.PickRandom();
                BoardTile tile = board.tiles[coords[0], coords[1]];
                if (tile.piece) tile.piece.Die();
                p.Move(tile);
                thinking = false;
                yield break;
            }
            timeOut -= Time.deltaTime;
            if (timeOut < 0)
            {
                Debug.LogWarning("Time out! Couldn't chose a piece to move!");
                thinking = false;
                yield break;
            }
            yield return null;
        }

        thinking = false;
    }

    private void Stop()
    {
        StopCoroutine(MovePieceCoroutine());
        thinking = false;
        enabled = false;
    }

    private void PieceDied(Piece p)
    {
        if (pieces.Contains(p))
            pieces.Remove(p);
    }
}
