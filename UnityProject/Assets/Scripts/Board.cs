using System;
using System.Collections.Generic;
using UnityEngine;

public class Board : Singleton<Board>
{
    [SerializeField]
    private BoardSettings settings;
    [SerializeField]
    private BoardLayout layout;

    public BoardTile[,] tiles;

    private Transform boardParent;
    private Transform pieceParent;
    private Transform selectionTile;
    private Piece selectedPiece;

    private List<BoardTile> possibleMoves = new List<BoardTile>();

    [Space(20), Header("Debug"), SerializeField]
    private bool debug = false;

    private void Awake()
    {
        EventManager.clickedOnTile.AddListener(ClickedOnTile);
        EventManager.enterTile.AddListener(EnterTile);
        EventManager.exitTile.AddListener(ExitTile);
        EventManager.pieceMoved.AddListener(PieceMoved);
        EventManager.startGame.AddListener(Init);
        EventManager.kingDied.AddListener((val) => EndGame());
        EventManager.clearBoard.AddListener(ClearBoard);
        EventManager.turnToQueen.AddListener(TurnToQueen);
    }

    private void ClearBoard()
    {
        if (boardParent)
            Destroy(boardParent.gameObject);
        if (pieceParent)
            Destroy(pieceParent.gameObject);
    }

    public void Init()
    {
        CreateBoard();
        CreatePieces();

        EventManager.boardInited.Invoke();
    }

    private void StartGame()
    {
        Init();
    }

    private void EndGame()
    {
        UIManager.SetPanel(Panel.EndScreen);
    }

    private void CreateBoard()
    {
        if (!boardParent)
            boardParent = new GameObject("Board").transform;
        if (!selectionTile)
            selectionTile = ResourceManager.GetObjectOfType<Transform>(settings.SelectionTile, Vector2.one * 100);

        tiles = new BoardTile[settings.width, settings.height];

        for (int j = 0; j < settings.height; j++)
            for (int i = 0; i < settings.width; i++)
            {
                tiles[i, j] = ResourceManager.GetObjectOfType<BoardTile>((i + j) % 2 == 0 ? settings.BlackTile : settings.WhiteTile, new Vector2(i, j));
                tiles[i, j].transform.SetParent(boardParent, true);
            }

        boardParent.transform.position = new Vector3(settings.xPos, settings.yPos, 0);
    }

    private void CheckTiles()
    {
        for (int i = 0; i < tiles.GetLength(0); i++)
            for (int j = 0; j < tiles.GetLength(1); j++)
                Debug.Log("Tile [" + i + ", " + j + "]: " + tiles[i, j].name);
    }

    private void ClickedOnTile(BoardTile tile)
    {
        if (tile.piece)
        {
            if ((!selectedPiece || tile.piece.side == selectedPiece.side) && tile.piece.side == settings.PlayerSide && tile.piece.cooldownTime <= 0)
            {
                selectedPiece = tile.piece;
                Highlight(tile.piece.PossiblePositions());
            }
            else if (selectedPiece && selectedPiece.side != tile.piece.side && possibleMoves.Contains(tile))
            {
                tile.piece.Die();
                selectedPiece.Move(tile);
                selectedPiece = null;
                ClearHighlight();
            }
        }
        else if (selectedPiece)
        {
            if (possibleMoves.Contains(tile))
            {
                selectedPiece.Move(tile);
                selectedPiece = null;
                ClearHighlight();
            }
        }
    }

    private void EnterTile(BoardTile tile)
    {
        selectionTile.position = tile != null ? tile.transform.position : Vector3.one * 100;
    }

    private void ExitTile(BoardTile tile)
    {

    }

    private void PieceMoved(Piece piece, BoardTile tile)
    {
        if (piece == selectedPiece)
            selectedPiece = null;
    }

    private void ClearHighlight()
    {
        for (int i = 0; i < tiles.GetLength(0); i++)
            for (int j = 0; j < tiles.GetLength(1); j++)
                tiles[i, j].Highlight(false);

        possibleMoves.Clear();
    }

    private void Highlight(int[][] coords)
    {
        ClearHighlight();
        for (int i = 0; i < coords.GetLength(0); i++)
            Highlight(coords[i]);
    }

    private void Highlight(int[] coords)
    {
        try
        {
            tiles[coords[0], coords[1]].Highlight(true);
            possibleMoves.Add(tiles[coords[0], coords[1]]);

        }
        catch (Exception e)
        {
            Debug.LogError("Something wrong happed during highlighting a tile at [" + coords[0] + ", " + coords[1] + "]. Tile: ");
            Debug.LogError(e);
        }
    }

    private BoardTile GetFreeTile()
    {
        for (int j = 0; j < tiles.GetLength(1); j++)
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                if (tiles[j, i].piece == null)
                    return tiles[i, j];
            }
        }
        Debug.LogError("No free tile is available! This should not be happenning!");
        return null;
    }

    private void CreatePieces()
    {
        if (!pieceParent)
            pieceParent = new GameObject("Piece Parent").transform;

        for (int i = 0; i < layout.pieces.Length; i++)
        {
            PieceStat stat = layout.pieces[i];
            if (!CheckForInstantiation(stat))
                continue;

            Piece p = ResourceManager.GetObjectOfType<Piece>(stat.prefab, Vector2.zero);
            p.Move(tiles[stat.xPos, stat.yPos]);
            p.Init(stat);
            p.Paint(stat.side == Side.White ? settings.WhiteTint : settings.BlackTint);

            if (p is Pawn)
                (p as Pawn).queenPos = stat.queenPos;

            p.transform.SetParent(pieceParent, true);
        }
    }

    private void TurnToQueen(Pawn pawn)
    {
        Queen q = ResourceManager.GetQueen();
        q.Move(tiles[pawn.xPos, pawn.yPos]);
        q.side = pawn.side;
        q.Paint(pawn.side == Side.White ? settings.WhiteTint : settings.BlackTint);
        q.transform.SetParent(pieceParent, true);
        Destroy(pawn.gameObject);
    }

    private bool CheckForInstantiation(PieceStat stat)
    {
        if (stat.xPos > settings.width)
        {
            Debug.LogError("X position of " + stat.name + " is too high: " + stat.xPos + ". Max: " + (settings.width - 1) + "! It will not be instantiated");
            return false;
        }
        else if (stat.xPos < 0)
        {
            Debug.LogError("X position cannot be less than 0! " + stat.name + " will not be instantiated!");
            return false;
        }

        if (stat.yPos > settings.height)
        {
            Debug.LogError("Y position of " + stat.name + " is too high: " + stat.xPos + ". Max: " + (settings.height - 1) + "! It will not be instantiated");
            return false;
        }
        else if (stat.yPos < 0)
        {
            Debug.LogError("Y position cannot be less than 0! " + stat.name + " will not be instantiated!");
            return false;
        }

        if (tiles[stat.xPos, stat.yPos].piece)
        {
            Debug.LogError("There is another piece at the [" + stat.xPos + ", " + stat.yPos + "]. " + stat.name + " will not be instantiated!");
            return false;
        }

        return true;
    }

    private void Update()
    {
        if (debug)
        {
            if (Input.GetKeyDown(KeyCode.Space))
                CheckTiles();
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                BoardTile t = GetFreeTile();
                if (t)
                {
                    Pawn p = ResourceManager.GetPawn();
                    p.Move(t);
                    t.piece = p;
                }
            }
            if (Input.GetKeyDown(KeyCode.F1))
            {
                BoardTile t = GetFreeTile();
                if (t)
                {
                    Pawn p = ResourceManager.GetPawn();
                    p.side = Side.Black;
                    p.Move(t);
                    t.piece = p;
                }
            }
        }
    }
}
