using System;
using UnityEngine;

[Serializable]
public class PieceStat
{
    public string name = "Piece";
    public GameObject prefab;
    public Side side = Side.White;
    public int xPos = 0;
    public int yPos = 0;
    public int queenPos = 7;
}

[CreateAssetMenu(fileName = "BoardLayout", menuName = "Chess/Board Layout")]
public class BoardLayout : ScriptableObject
{
    public PieceStat[] pieces;
}
