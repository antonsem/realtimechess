using UnityEngine;

[CreateAssetMenu(fileName = "BoardSettings", menuName = "Chess/Board Settings")]
public class BoardSettings : ScriptableObject
{

    public int width = 8;
    public int height = 8;

    public float xPos = -4;
    public float yPos = -4;

    [SerializeField]
    private Side playerSide = Side.White;
    public Side PlayerSide
    {
        get { return playerSide; }
    }

    [SerializeField]
    private GameObject blackTile;
    public GameObject BlackTile
    {
        get { return blackTile; }
    }

    [SerializeField]
    private GameObject whiteTile;
    public GameObject WhiteTile
    {
        get { return whiteTile; }
    }

    [SerializeField]
    private GameObject selectionTile;
    public GameObject SelectionTile
    {
        get { return selectionTile; }
    }

    [Space(20), Header("Cosmetics"), SerializeField]
    private Color whiteTint;
    public Color WhiteTint
    {
        get { return whiteTint; }
    }

    [SerializeField]
    private Color blackTint;
    public Color BlackTint
    {
        get { return blackTint; }
    }
}
