using UnityEngine;
using UnityEngine.UI;

public class Piece : MonoBehaviour
{
    public int xPos = 0;
    public int yPos = 0;

    public Side side = Side.White;

    public int cost = 1;

    public float cooldownTime = 1;
    private float defaultCooldownTime = 1;

    [SerializeField]
    private Image cooldownSlider;

    private SpriteRenderer rend;

    protected Board board;

    private void Awake()
    {
        board = Board.Instance;
        Utils.SetComponent(out rend, transform, true);
        defaultCooldownTime = cooldownTime;
    }

    public virtual void Die()
    {
        EventManager.pieceDied.Invoke(this);
        Destroy(gameObject);
    }

    public void Paint(Color c)
    {
        rend.color = c;
    }

    public virtual void Init(PieceStat stat)
    {
        side = stat.side;
    }

    public virtual void Move(BoardTile tile)
    {
        cooldownTime = defaultCooldownTime;
        transform.position = tile.transform.position;
        xPos = (int)tile.transform.localPosition.x;
        yPos = (int)tile.transform.localPosition.y;
        tile.piece = this;
        EventManager.pieceMoved.Invoke(this, tile);
    }

    public virtual bool CanMoveTo(BoardTile tile)
    {
        if (tile.piece == null)
            return true;

        return false;
    }

    public virtual int[][] PossiblePositions()
    {
        int[][] retVal = new int[64][];

        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                retVal[(i * 8) + j] = new int[2] { i, j };

        return retVal;
    }

    private void Update()
    {
        if (cooldownTime > 0)
        {
            cooldownTime -= Time.deltaTime;
            cooldownSlider.fillAmount = cooldownTime / defaultCooldownTime;
        }
    }
}
