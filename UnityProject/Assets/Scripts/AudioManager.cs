using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : Singleton<AudioManager>
{
    private static AudioSource source;
    [SerializeField]
    private GameStats stats;
    [SerializeField]
    private AudioClip move;
    [SerializeField]
    private AudioClip die;
    [SerializeField]
    private AudioClip click;
    [SerializeField]
    private AudioClip select;

    private void Awake()
    {
        Utils.SetComponent(out source, transform);
        EventManager.pieceMoved.AddListener(PieceMoved);
        EventManager.pieceDied.AddListener(PieceDied);
        EventManager.clickedOnTile.AddListener((val) => Select());
    }

    public void Click()
    {
        PlayOneShot(click);
    }

    public static void PlayOneShot(AudioClip clip, float pitchMin = 1, float pitchMax = 1)
    {
        source.pitch = Random.Range(pitchMin, pitchMax);
        source.PlayOneShot(clip);
    }

    private void Select()
    {
        if (stats.inGame)
        {
            source.pitch = Random.Range(0.9f, 1.1f);
            source.PlayOneShot(select);
        }
    }

    private void PieceMoved(Piece piece, BoardTile tile)
    {
        if (stats.inGame)
        {
            source.pitch = Random.Range(0.9f, 1.1f);
            source.PlayOneShot(move);
        }
    }

    private void PieceDied(Piece piece)
    {
        if (stats.inGame)
        {
            source.pitch = Random.Range(0.9f, 1.1f);
            source.PlayOneShot(die);
        }
    }
}
