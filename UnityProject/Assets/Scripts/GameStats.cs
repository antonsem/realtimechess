using UnityEngine;

[CreateAssetMenu(fileName = "GameStats", menuName = "Chess/Game Stats")]
public class GameStats : ScriptableObject
{
    public bool inGame = false;
    public float gameTime = 0;
    public int playerScore = 0;
    public int cpuScore = 0;
    public Side fallenKing = Side.None;

    public void Reset()
    {
        inGame = false;
        gameTime = 0;
        playerScore = 0;
        cpuScore = 0;
        fallenKing = Side.None;
    }
}
