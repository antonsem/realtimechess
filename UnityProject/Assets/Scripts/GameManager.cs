using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public GameStats stats;
    public BoardSettings settings;

    private void Awake()
    {
        EventManager.startGame.AddListener(ResetStats);
        EventManager.boardInited.AddListener(StartGame);
        EventManager.kingDied.AddListener(EndGame);
        EventManager.pieceDied.AddListener(PieceDied);
    }

    private void ResetStats()
    {
        stats.Reset();
    }

    private void StartGame()
    {
        stats.inGame = true;
    }

    private void EndGame(Side loosingSide)
    {
        stats.fallenKing = loosingSide;
        stats.inGame = false;
    }

    private void PieceDied(Piece piece)
    {
        if (piece.side == settings.PlayerSide)
            stats.cpuScore += piece.cost;
        else
            stats.playerScore += piece.cost;

        CameraShake.Instance.ShakeCamera(0.1f, 0.01f);
    }

    private void Update()
    {
        if(stats.inGame)
        {
            stats.gameTime += Time.deltaTime;
        }
    }
}
