using UnityEngine.Events;

public class ClickedOnTile : UnityEvent<BoardTile>
{ }

public class EnterTile : UnityEvent<BoardTile>
{ }

public class ExitTile : UnityEvent<BoardTile>
{ }

public class PieceDied : UnityEvent<Piece>
{ }

public class PieceMoved : UnityEvent<Piece, BoardTile>
{ }

public class GameEnded : UnityEvent<Side>
{ }

public class StartGame : UnityEvent
{ }

public class BoardInited : UnityEvent
{ }

public class ClearBoard : UnityEvent
{ }

public class TurnToQueen : UnityEvent<Pawn>
{ }

public static class EventManager
{
    public static ClickedOnTile clickedOnTile = new ClickedOnTile();
    public static EnterTile enterTile = new EnterTile();
    public static ExitTile exitTile = new ExitTile();
    public static PieceMoved pieceMoved = new PieceMoved();
    public static PieceDied pieceDied = new PieceDied();
    public static GameEnded kingDied = new GameEnded();
    public static StartGame startGame = new StartGame();
    public static BoardInited boardInited = new BoardInited();
    public static ClearBoard clearBoard = new ClearBoard();
    public static TurnToQueen turnToQueen = new TurnToQueen();
}
