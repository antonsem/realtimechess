using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Button newGame;
    [SerializeField]
    private Button credits;

    void Start()
    {
        newGame.onClick.AddListener(OnNewGame);
        credits.onClick.AddListener(OnCredits);
    }

    private void OnEnable()
    {
        EventManager.clearBoard.Invoke();
    }

    private void OnCredits()
    {
        UIManager.SetPanel(Panel.Credits);
    }

    private void OnNewGame()
    {
        EventManager.startGame.Invoke();
        AudioManager.Instance.Click();
        UIManager.SetPanel(Panel.InGame);
    }
}
