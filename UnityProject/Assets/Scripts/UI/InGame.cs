using UnityEngine;
using UnityEngine.UI;

public class InGame : MonoBehaviour
{
    [SerializeField]
    private GameStats stats;
    [SerializeField]
    private Text time;
    [SerializeField]
    private Text playerScore;
    [SerializeField]
    private Text cpuScore;

    void Update()
    {
        time.text = stats.gameTime.ToString("0.0");
        playerScore.text = stats.playerScore.ToString();
        cpuScore.text = stats.cpuScore.ToString();
    }
}
