using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    [SerializeField]
    private Button mainMenu;

    void Start()
    {
        mainMenu.onClick.AddListener(OnMainMenu);
    }

    private void OnMainMenu()
    {
        UIManager.SetPanel(Panel.MainMenu);
    }
}
