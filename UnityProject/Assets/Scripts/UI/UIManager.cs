using UnityEngine;

public enum Panel
{
    MainMenu,
    EndScreen,
    InGame,
    Credits,
    None
}

public class UIManager : Singleton<UIManager>
{
    public Panel defaultPanel;

    private static MainMenu mainMenu;
    private static EndScreen endScreen;
    private static InGame inGame;
    private static Credits credits;

    public static Panel currentPanel = Panel.MainMenu;

    void Start()
    {
        Utils.SetObject(out mainMenu);
        Utils.SetObject(out endScreen);
        Utils.SetObject(out inGame);
        Utils.SetObject(out credits);

        SetPanel(defaultPanel);
    }

    public static void SetPanel(Panel type)
    {
        mainMenu.gameObject.SetActive(type == Panel.MainMenu);
        endScreen.gameObject.SetActive(type == Panel.EndScreen);
        inGame.gameObject.SetActive(type == Panel.InGame);
        credits.gameObject.SetActive(type == Panel.Credits);

        currentPanel = type;
    }
}
