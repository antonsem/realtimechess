using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour
{
    [SerializeField]
    private GameStats stats;
    [SerializeField]
    private BoardSettings settings;
    [SerializeField]
    private Button mainMenu;
    [SerializeField]
    private Text gameTime;
    [SerializeField]
    private Text playerScore;
    [SerializeField]
    private Text cpuScore;
    [SerializeField]
    private Text fallenKing;

    void Start()
	{
        mainMenu.onClick.AddListener(OnMainMenu);
	}

    private void OnEnable()
    {
        gameTime.text = stats.gameTime.ToString("0.0");
        playerScore.text = stats.playerScore.ToString();
        cpuScore.text = stats.cpuScore.ToString();
        fallenKing.text = settings.PlayerSide != stats.fallenKing ? "CPU" : "Player";
    }

    private void OnMainMenu()
    {
        AudioManager.Instance.Click();
        UIManager.SetPanel(Panel.MainMenu);
    }
}
