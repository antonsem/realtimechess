using System.Collections.Generic;
using UnityEngine;

public class Knight : Piece
{
    private List<Vector2> possibleMoves = new List<Vector2>();

    public override int[][] PossiblePositions()
    {
        possibleMoves.Clear();

        if (xPos + 2 < board.tiles.GetLength(0))
        {
            if (yPos + 1 < board.tiles.GetLength(1) && (!board.tiles[xPos + 2, yPos + 1].piece || board.tiles[xPos + 2, yPos + 1].piece.side != side))
                possibleMoves.Add(new Vector2(xPos + 2, yPos + 1));
            if (yPos - 1 >= 0 && (!board.tiles[xPos + 2, yPos - 1].piece || board.tiles[xPos + 2, yPos - 1].piece.side != side))
                possibleMoves.Add(new Vector2(xPos + 2, yPos - 1));
        }

        if (xPos - 2 >= 0)
        {
            if (yPos + 1 < board.tiles.GetLength(1) && (!board.tiles[xPos - 2, yPos + 1].piece || board.tiles[xPos - 2, yPos + 1].piece.side != side))
                possibleMoves.Add(new Vector2(xPos - 2, yPos + 1));
            if (yPos - 1 >= 0 && (!board.tiles[xPos - 2, yPos - 1].piece || board.tiles[xPos - 2, yPos - 1].piece.side != side))
                possibleMoves.Add(new Vector2(xPos - 2, yPos - 1));
        }

        if (yPos + 2 < board.tiles.GetLength(1))
        {
            if (xPos + 1 < board.tiles.GetLength(0) && (!board.tiles[xPos + 1, yPos + 2].piece || board.tiles[xPos + 1, yPos + 2].piece.side != side))
                possibleMoves.Add(new Vector2(xPos + 1, yPos + 2));
            if (xPos - 1 >= 0 && (!board.tiles[xPos - 1, yPos + 2].piece || board.tiles[xPos - 1, yPos + 2].piece.side != side))
                possibleMoves.Add(new Vector2(xPos - 1, yPos + 2));
        }

        if (yPos - 2 >= 0)
        {
            if (xPos + 1 < board.tiles.GetLength(0) && (!board.tiles[xPos + 1, yPos - 2].piece || board.tiles[xPos + 1, yPos - 2].piece.side != side))
                possibleMoves.Add(new Vector2(xPos + 1, yPos - 2));
            if (xPos - 1 >= 0 && (!board.tiles[xPos - 1, yPos - 2].piece || board.tiles[xPos - 1, yPos - 2].piece.side != side))
                possibleMoves.Add(new Vector2(xPos - 1, yPos - 2));
        }

        int[][] retVal = new int[possibleMoves.Count][];
        for (int k = 0; k < possibleMoves.Count; k++)
            retVal[k] = new int[] { (int)possibleMoves[k].x, (int)possibleMoves[k].y };

        return retVal;
    }
}
