using System.Collections.Generic;
using UnityEngine;

public class King : Piece
{
    private List<Vector2> possibleMoves = new List<Vector2>();

    public override int[][] PossiblePositions()
    {
        possibleMoves.Clear();

        if (xPos + 1 < board.tiles.GetLength(0) && (!board.tiles[xPos + 1, yPos].piece || board.tiles[xPos + 1, yPos].piece.side != side))
        {
            possibleMoves.Add(new Vector2(xPos + 1, yPos));
            if (yPos + 1 < board.tiles.GetLength(1) && (!board.tiles[xPos + 1, yPos + 1].piece || board.tiles[xPos + 1, yPos + 1].piece.side != side))
                possibleMoves.Add(new Vector2(xPos + 1, yPos + 1));
            if (yPos - 1 >= 0 && (!board.tiles[xPos + 1, yPos - 1].piece || board.tiles[xPos + 1, yPos - 1].piece.side != side))
                possibleMoves.Add(new Vector2(xPos + 1, yPos - 1));
        }
        if (xPos - 1 >= 0 && (!board.tiles[xPos - 1, yPos].piece || board.tiles[xPos - 1, yPos].piece.side != side))
        {
            possibleMoves.Add(new Vector2(xPos - 1, yPos));
            if (yPos + 1 < board.tiles.GetLength(1) && (!board.tiles[xPos - 1, yPos + 1].piece || board.tiles[xPos - 1, yPos + 1].piece.side != side))
                possibleMoves.Add(new Vector2(xPos - 1, yPos + 1));
            if (yPos - 1 >= 0 && (!board.tiles[xPos - 1, yPos - 1].piece || board.tiles[xPos - 1, yPos - 1].piece.side != side))
                possibleMoves.Add(new Vector2(xPos - 1, yPos - 1));
        }
        if (yPos + 1 < board.tiles.GetLength(1) && (!board.tiles[xPos, yPos + 1].piece || board.tiles[xPos, yPos + 1].piece.side != side))
            possibleMoves.Add(new Vector2(xPos, yPos + 1));
        if (yPos - 1 >= 0 && (!board.tiles[xPos, yPos - 1].piece || board.tiles[xPos, yPos - 1].piece.side != side))
            possibleMoves.Add(new Vector2(xPos, yPos - 1));

        int[][] retVal = new int[possibleMoves.Count][];
        for (int i = 0; i < possibleMoves.Count; i++)
            retVal[i] = new int[] { (int)possibleMoves[i].x, (int)possibleMoves[i].y };

        return retVal;
    }

    public override void Die()
    {
        EventManager.kingDied.Invoke(side);
        base.Die();
    }
}
