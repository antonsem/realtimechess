using System.Collections.Generic;
using UnityEngine;

public class Queen : Piece
{
    private List<Vector2> possibleMoves = new List<Vector2>();

    public override int[][] PossiblePositions()
    {
        possibleMoves.Clear();

        int i = xPos + 1;
        int j = yPos + 1;

        while (i < board.tiles.GetLength(0) && j < board.tiles.GetLength(1))
        {
            if (!board.tiles[i, j].piece || board.tiles[i, j].piece.side != side)
                possibleMoves.Add(new Vector2(i, j));
            if (board.tiles[i, j].piece)
                break;

            i++;
            j++;
        }

        i = xPos - 1;
        j = yPos + 1;

        while (i >= 0 && j < board.tiles.GetLength(1))
        {
            if (!board.tiles[i, j].piece || board.tiles[i, j].piece.side != side)
                possibleMoves.Add(new Vector2(i, j));
            if (board.tiles[i, j].piece)
                break;

            i--;
            j++;
        }

        i = xPos - 1;
        j = yPos - 1;

        while (i >= 0 && j >= 0)
        {
            if (!board.tiles[i, j].piece || board.tiles[i, j].piece.side != side)
                possibleMoves.Add(new Vector2(i, j));
            if (board.tiles[i, j].piece)
                break;

            i--;
            j--;
        }

        i = xPos + 1;
        j = yPos - 1;

        while (i < board.tiles.GetLength(0) && j >= 0)
        {
            if (!board.tiles[i, j].piece || board.tiles[i, j].piece.side != side)
                possibleMoves.Add(new Vector2(i, j));
            if (board.tiles[i, j].piece)
                break;

            i++;
            j--;
        }

        for (int k = xPos + 1; k < board.tiles.GetLength(0); k++)
        {
            if (!board.tiles[k, yPos].piece || board.tiles[k, yPos].piece.side != side)
                possibleMoves.Add(new Vector2(k, yPos));
            if (board.tiles[k, yPos].piece)
                break;
        }

        for (int k = xPos - 1; k >= 0; k--)
        {
            if (!board.tiles[k, yPos].piece || board.tiles[k, yPos].piece.side != side)
                possibleMoves.Add(new Vector2(k, yPos));
            if (board.tiles[k, yPos].piece)
                break;
        }

        for (int k = yPos + 1; k < board.tiles.GetLength(1); k++)
        {
            if (!board.tiles[xPos, k].piece || board.tiles[xPos, k].piece.side != side)
                possibleMoves.Add(new Vector2(xPos, k));
            if (board.tiles[xPos, k].piece)
                break;
        }

        for (int k = yPos - 1; k >= 0; k--)
        {
            if (!board.tiles[xPos, k].piece || board.tiles[xPos, k].piece.side != side)
                possibleMoves.Add(new Vector2(xPos, k));
            if (board.tiles[xPos, k].piece)
                break;
        }

        int[][] retVal = new int[possibleMoves.Count][];
        for (int k = 0; k < possibleMoves.Count; k++)
            retVal[k] = new int[] { (int)possibleMoves[k].x, (int)possibleMoves[k].y };

        return retVal;
    }
}
