using System.Collections.Generic;
using UnityEngine;

public class Bishop : Piece
{
    private List<Vector2> possibleMoves = new List<Vector2>();

    public override int[][] PossiblePositions()
    {
        possibleMoves.Clear();

        int i = xPos + 1;
        int j = yPos + 1;

        while(i < board.tiles.GetLength(0) && j < board.tiles.GetLength(1))
        {
            if (!board.tiles[i, j].piece || board.tiles[i, j].piece.side != side)
                possibleMoves.Add(new Vector2(i, j));
            if (board.tiles[i, j].piece)
                break;

            i++;
            j++;
        }

        i = xPos - 1;
        j = yPos + 1;

        while (i >= 0 && j < board.tiles.GetLength(1))
        {
            if (!board.tiles[i, j].piece || board.tiles[i, j].piece.side != side)
                possibleMoves.Add(new Vector2(i, j));
            if (board.tiles[i, j].piece)
                break;

            i--;
            j++;
        }

        i = xPos - 1;
        j = yPos - 1;

        while (i >= 0 && j >= 0)
        {
            if (!board.tiles[i, j].piece || board.tiles[i, j].piece.side != side)
                possibleMoves.Add(new Vector2(i, j));
            if (board.tiles[i, j].piece)
                break;

            i--;
            j--;
        }

        i = xPos + 1;
        j = yPos - 1;

        while (i < board.tiles.GetLength(0) && j >= 0)
        {
            if (!board.tiles[i, j].piece || board.tiles[i, j].piece.side != side)
                possibleMoves.Add(new Vector2(i, j));
            if (board.tiles[i, j].piece)
                break;

            i++;
            j--;
        }

        int[][] retVal = new int[possibleMoves.Count][];
        for (int k = 0; k < possibleMoves.Count; k++)
            retVal[k] = new int[] { (int)possibleMoves[k].x, (int)possibleMoves[k].y };

        return retVal;
    }
}
