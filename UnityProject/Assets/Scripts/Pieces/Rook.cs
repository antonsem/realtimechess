using System.Collections.Generic;
using UnityEngine;

public class Rook : Piece
{
    private List<Vector2> possibleMoves = new List<Vector2>();

    public override int[][] PossiblePositions()
    {
        possibleMoves.Clear();

        for (int i = xPos + 1; i < board.tiles.GetLength(0); i++)
        {
            if (!board.tiles[i, yPos].piece || board.tiles[i, yPos].piece.side != side)
                possibleMoves.Add(new Vector2(i, yPos));
            if (board.tiles[i, yPos].piece)
                break;
        }

        for (int i = xPos - 1; i >= 0; i--)
        {
            if (!board.tiles[i, yPos].piece || board.tiles[i, yPos].piece.side != side)
                possibleMoves.Add(new Vector2(i, yPos));
            if (board.tiles[i, yPos].piece)
                break;
        }

        for (int i = yPos + 1; i < board.tiles.GetLength(1); i++)
        {
            if (!board.tiles[xPos, i].piece || board.tiles[xPos, i].piece.side != side)
                possibleMoves.Add(new Vector2(xPos, i));
            if (board.tiles[xPos, i].piece)
                break;
        }

        for (int i = yPos - 1; i >= 0; i--  )
        {
            if (!board.tiles[xPos, i].piece || board.tiles[xPos, i].piece.side != side)
                possibleMoves.Add(new Vector2(xPos, i));
            if (board.tiles[xPos, i].piece)
                break;
        }

        int[][] retVal = new int[possibleMoves.Count][];
        for (int i = 0; i < possibleMoves.Count; i++)
            retVal[i] = new int[] { (int)possibleMoves[i].x, (int)possibleMoves[i].y };

        return retVal;
    }
}
