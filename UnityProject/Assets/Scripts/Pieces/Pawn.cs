using System.Collections.Generic;
using UnityEngine;

public class Pawn : Piece
{
    private bool moved = false;
    private List<Vector2> possibleMoves = new List<Vector2>();
    private int moveDirection = 1;
    public int queenPos = 7;

    private void Start()
    {
        moved = false;
    }

    public override void Init(PieceStat stat)
    {
        base.Init(stat);
        moved = false;
        moveDirection = stat.side == Side.White ? 1 : -1;
    }

    public override int[][] PossiblePositions()
    {
        possibleMoves.Clear();

        for (int i = 0; i < board.tiles.GetLength(0); i++)
        {
            for (int j = 0; j < board.tiles.GetLength(1); j++)
            {
                if (j == yPos + moveDirection)
                {
                    if (i == xPos && !board.tiles[i, j].piece)
                        possibleMoves.Add(new Vector2(i, j));
                    else if ((i == xPos - moveDirection || i == xPos + moveDirection) && board.tiles[i, j].piece && board.tiles[i, j].piece.side != side)
                        possibleMoves.Add(new Vector2(i, j));
                }
                else if (j == yPos + 2 * moveDirection && !moved)
                {
                    if (i == xPos && !board.tiles[i, j].piece && !board.tiles[i, j - moveDirection].piece)
                        possibleMoves.Add(new Vector2(i, j));
                }
            }
        }

        int[][] retVal = new int[possibleMoves.Count][];
        for (int i = 0; i < possibleMoves.Count; i++)
            retVal[i] = new int[] { (int)possibleMoves[i].x, (int)possibleMoves[i].y };

        return retVal;
    }

    public override void Move(BoardTile tile)
    {
        base.Move(tile);
        moved = true;
        if (yPos == queenPos)
            EventManager.turnToQueen.Invoke(this);
    }
}
